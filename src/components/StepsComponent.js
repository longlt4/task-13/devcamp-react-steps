import React from "react";
class Steps extends React.Component {
    render() {
        let { id, title, content } = this.props;

        return (
            <div>
                Các bước {id}: {title} {content} 
            </div>
        )
    }
}

export default Steps;